# Keyboard backlight contol

This repo is mainly the home of [kb-backlight](kb-backlight), a python script to control the brightness and color (if supported) of a keyboard backlight on Linux. Brightness control *should* work on all systems. Color control has currently only been tested on the System76 Oryx Pro running Pop!_OS. If you run Linux on a computer with a multicolored keyboard backlight, please let me know if it works and report any issues.
